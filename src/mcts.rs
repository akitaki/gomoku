use std::{fmt::Debug, marker::PhantomData};

use anyhow::anyhow;
use anyhow::{Context, Result};
use indextree::{Arena, NodeId};
use ndarray::prelude::*;
use rand::prelude::*;
use tracing::info;

use super::{Solver, State};

/// The state type `S` is required to be `Clone`.
pub struct MctsSolver;

#[derive(Debug)]
struct Data<S>
where
    S: State,
{
    visits: usize,
    value: f32,
    state: S,
    last_action: Option<S::A>,
    depth: usize,
}

impl<S> Solver<S> for MctsSolver
where
    S: State,
{
    // wrapper of the real implementation
    fn solve(&mut self, state: &S) -> S::A {
        Mcts::<S> { _s: PhantomData }.solve(state).unwrap().0
    }
}

#[allow(dead_code)]
pub fn dbg() -> Result<()> {
    use super::game::*;
    let board = ascii_to_board(
        r#" oo o
#
#
#"#,
    );
    let (_answer, candidates) = Mcts::<Board> { _s: PhantomData }.solve(&board)?;

    let mut heatmap = Array2::<f32>::zeros((5, 5));
    for (action, visits) in candidates.unwrap().into_iter() {
        heatmap[action] = visits as f32;
    }
    let sum: f32 = heatmap.iter().sum();
    for visits in heatmap.iter_mut() {
        *visits /= sum;
    }

    ndarray_npy::write_npy("heatmap.npy", &heatmap)?;
    Ok(())
}

struct Mcts<S>
where
    S: State,
{
    _s: PhantomData<S>,
}

impl<S> Mcts<S>
where
    S: State,
{
    // real implementation
    pub fn solve(&mut self, state: &S) -> Result<(S::A, Option<Vec<(S::A, usize)>>)> {
	let start = chrono::Utc::now();

        let arena = &mut Arena::<Data<S>>::new();
        let me = state.next();

        let id_root = arena.new_node(Data {
            visits: 0,
            value: 0.0,
            state: state.clone(),
            last_action: None,
            depth: 0,
        });

        for _ in 0..10000 {
            // select
            let mut current = id_root;
            while Self::has_children(arena, current) {
                let selected = Self::select(&arena, current)?;
                current = selected;
            }

            // expand
            if !Self::is_ended(arena, current) {
                Self::expand(arena, current);

                // select once more
                if Self::has_children(arena, current) {
                    current = Self::select(&arena, current)?;
                }
            }

            // simulate
            let sim_start_state = Self::state_of(arena, current);
            let (sim_result, sim_steps) = Self::simulate(sim_start_state.clone())?;
            let sim_score = match sim_result {
                Some(player) => {
                    if player == me {
                        1.0
                    } else {
                        let depth = Self::depth_of(arena, current) + sim_steps;
                        100.0 * (f32::atan(depth as f32) * 2.0 / std::f32::consts::PI - 1.0)
                    }
                }
                None => 0.5,
            };

            // backprop
            loop {
                let node = arena.get_mut(current).unwrap();
                let data = node.get_mut();
                data.visits += 1;
                data.value += sim_score;

                if let Some(parent) = node.parent() {
                    current = parent;
                } else {
                    break;
                }
            }
        }

        let mut candidates = id_root
            .children(arena)
            .map(|child| arena.get(child).unwrap().get())
            .collect::<Vec<&Data<S>>>();
        candidates.sort_by_key(|data| data.visits);
        candidates.reverse();

        let answer = candidates[0]
            .last_action
            .clone()
            .context("Top candidate doesn't have last action")?;

        let candidates = candidates
            .into_iter()
            .filter_map(|data| data.last_action.clone().map(|action| (action, data.visits)))
            .collect::<Vec<(S::A, usize)>>();

	let end = chrono::Utc::now();
	let elapsed = end - start;
	info!("search completed in {}", elapsed.num_seconds());

        Ok((answer, Some(candidates)))
    }

    fn simulate(state: S) -> Result<(Option<S::P>, usize)> {
        let mut state = state;
        let mut steps = 0;
        while !state.is_ended() && state.winner().is_none() {
            steps += 1;
            let action = state
                .get_legal_actions()
                .choose(&mut thread_rng())
                .context("No legal action")?;
            state.apply(action);
        }

        // either it's ended or there's a winner
        Ok((state.winner(), steps))
    }

    fn has_children(arena: &Arena<Data<S>>, id: NodeId) -> bool {
        id.children(arena).next().is_some()
    }

    fn is_ended(arena: &Arena<Data<S>>, id: NodeId) -> bool {
        arena.get(id).unwrap().get().state.is_ended()
    }

    fn state_of(arena: &Arena<Data<S>>, id: NodeId) -> &S {
        &arena.get(id).unwrap().get().state
    }

    fn depth_of(arena: &Arena<Data<S>>, id: NodeId) -> usize {
        arena.get(id).unwrap().get().depth
    }

    // should be called only when `id` has child
    fn select(arena: &Arena<Data<S>>, id: NodeId) -> Result<NodeId> {
        let Data {
            visits: parent_visits,
            ..
        } = *arena.get(id).unwrap().get();
        let parent_visits = parent_visits as f32;

        let children_and_scores = id.children(arena).map(|child| {
            let Data {
                visits: child_visits,
                value,
                ..
            } = *arena.get(child).unwrap().get();
            let child_visits = child_visits as f32;

            let exploit = value / (child_visits + 1.0);
            let explore = f32::sqrt(2.0 * f32::ln(parent_visits.max(1.0)) / (child_visits + 1.0));
            let ucb = exploit + explore;

            if ucb.is_nan() {
                eprintln!("is nan");
            }

            (child, ucb)
        });

        // TODO: Randomly choose when there are ties
        let mut selected = None;
        let mut max_score = f32::NEG_INFINITY;
        for (child, score) in children_and_scores {
            if score.is_nan() {
                return Err(anyhow!("Some score is NaN"));
            }
            if score >= max_score {
                max_score = score;
                selected.insert(child);
            }
        }

        selected.context("No child selected")
    }

    // prerequisite: id has a child
    fn expand(arena: &mut Arena<Data<S>>, id: NodeId) {
        let Data {
            state: from_state,
            depth: from_depth,
            ..
        } = arena.get(id).unwrap().get();
        let from_state = from_state.clone();
        let from_depth = from_depth.clone();
        let actions = from_state.get_legal_actions().collect::<Vec<_>>();

        for action in actions.into_iter() {
            // create new state after the action
            let mut to_state = from_state.clone();
            to_state.apply(action.clone());

            let id_child = arena.new_node(Data {
                visits: 0,
                value: 0.0,
                state: to_state,
                depth: from_depth + 1,
                last_action: Some(action),
            });

            // connect to its parent
            id.append(id_child, arena);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{Action, Player};

    #[test]
    fn has_children_test() {
        let arena = &mut Arena::new();
        let id = arena.new_node(dummy_data());
        assert_eq!(Mcts::<TestState>::has_children(arena, id), false);

        id.append(arena.new_node(dummy_data()), arena);
        assert_eq!(Mcts::<TestState>::has_children(arena, id), true);
    }

    fn dummy_data() -> Data<TestState> {
        Data {
            state: TestState,
            last_action: None,
            value: 0.0,
            visits: 0,
            depth: 0,
        }
    }

    #[derive(Debug, Clone)]
    struct TestState;
    #[derive(Debug, Clone)]
    struct TestAction;
    #[derive(Debug, Clone, PartialEq, Eq)]
    struct TestPlayer;

    impl State for TestState {
        type A = TestAction;
        type P = TestPlayer;

        fn get_legal_actions(&self) -> Box<dyn Iterator<Item = TestAction> + '_> {
            Box::new(std::iter::once(TestAction))
        }

        // this doesn't matter
        fn is_ended(&self) -> bool {
            false
        }

        fn apply(&mut self, _action: TestAction) {}

        fn next(&self) -> Self::P {
            TestPlayer
        }

        fn winner(&self) -> Option<Self::P> {
            None
        }
    }

    impl Action for TestAction {}
    impl Player for TestPlayer {}
}
