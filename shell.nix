{
  pkgs ? import <nixpkgs> {
    # Rust overlay
    overlays = [
      (import (builtins.fetchTarball {
        url = https://github.com/oxalica/rust-overlay/archive/master.tar.gz;
      }))
    ];
    config.allowUnfree = true;
  },
  lib ? pkgs.stdenv.lib 
}:

pkgs.mkShell rec {
  buildInputs = with pkgs; [
    # for rust -sys crates
    openssl pkg-config gcc
    # completion
    tabnine
    # gui
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    vulkan-loader
    # rust
    rust-analyzer cargo-watch gdb nodejs llvmPackages.lld
    python3Packages.numpy python3Packages.ipython python3Packages.matplotlib
    (rust-bin.stable.latest.default.override {
      extensions = [ "rust-src" ];
    })
  ];

  LD_LIBRARY_PATH = "${lib.makeLibraryPath buildInputs}";

  shellHook = ''
    rm -f rust-analyzer && ln -s ${pkgs.rust-analyzer}/bin/rust-analyzer rust-analyzer
  '';
}
