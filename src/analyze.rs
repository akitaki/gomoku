use indicatif::ParallelProgressIterator;
use rayon::prelude::*;
use tracing::info;

use crate::{game, math, mcts, Solver};

#[allow(dead_code)]
fn avg_moves(board_size: usize, num_threads: usize, times: usize) -> anyhow::Result<()> {
    rayon::ThreadPoolBuilder::new()
        .num_threads(num_threads)
        .build_global()?;

    let mut ages = vec![];
    (0..times)
        .into_par_iter()
        .progress()
        .map(|_| {
            let mut board = game::Board::new(board_size);
            while !board.is_ended() {
                let action = mcts::MctsSolver.solve(&board);
                board.place(action);
            }

            board.age() as i32
        })
        .collect_into_vec(&mut ages);

    let (mean, stdev) = (
        math::mean(&ages).unwrap(),
        math::std_deviation(&ages).unwrap(),
    );
    info!("{} (+- {})", mean, stdev);

    Ok(())
}
